@extends('layouts.base.app')
@section('title', ' Edit Profil Petugas')

@section('sidebar')
    @include('layouts.base.sidebar')
@endsection

@section('header')
    @include('layouts.base.header')
@endsection

@section('content')
<style>
  #map {
    height: 100%;
  }
  html, body {
    height: 100%;
    margin: 0;
    padding: 0;
  }
</style>
<div class="col-12 mb-2 mt-2">
    @if($this->session->flashdata('message')) 
        @if($this->session->flashdata('message')['type'] == 'error')
        <div class="alert alert-danger">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @else
        <div class="alert alert-success">
            {{ implode('\n', $this->session->flashdata('message')['message']) }}
        </div>
        @endif
    @endif
</div>

<nav class="breadcrumb bg-white push">
    <a class="breadcrumb-item" href="{{ base_url('/') }}">Dashboard</a>
    <a class="breadcrumb-item" href="{{ base_url('PetugasController/indexProfil') }}">Profil</a>
    <span class="breadcrumb-item active">Edit Profil</span>
</nav>
<div class="block">
    <div class="block-header block-header-default bg-primary">
        <h3 class="block-title">Edit Profil</h3>
    </div>
    <div class="block-content">
        <form class="js-validation-signup px-30" method="POST" enctype="multipart/form-data" action="{{ base_url('PetugasController/editProfil') }}" aria-label="">
            <div class="form-group row">
                @if($petugas->foto_ktp)
                    <!-- <img src="{{ base_url('img/profil/').$petugas->foto }}" alt="" class="img-fluid rounded mx-auto d-bloc" style="width: 400px;"> -->
                    <img src="{{ base_url('img/profil/user.jpeg') }}" alt="" class="img-fluid rounded mx-auto d-bloc" style="width: 400px;">
                @else
                    <img src="{{ base_url('img/profil/user.jpeg') }}" alt="" class="img-fluid rounded mx-auto d-bloc" style="width: 400px;">
                @endif
            </div>
            <div class="form-group row">
                <div class="col-12">
                    <div class="form-material form-material-primary floating input-group">
                        <input value="{{ $petugas->nama }}" type="text" class="form-control" id="nama" name="nama" readonly>
                        <label for="nama">Nama</label>
                        <div class="input-group-append">
                            <span class="input-group-text">Contoh: Hikmawan</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-12">
                    <div class="form-material form-material-primary floating input-group">
                        <input value="{{ $petugas->email }}" type="text" class="form-control" id="email" name="email" readonly>
                        <label for="email">Email</label>
                        <div class="input-group-append">
                            <span class="input-group-text">Contoh: hikmawan@gmail.com</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-12">
                    <div class="form-material form-material-primary floating input-group">
                        <input value="{{ $petugas->no_telp }}" type="text" class="form-control" id="telp" name="telp">
                        <label for="telp">Nomer Telepon</label>
                        <div class="input-group-append">
                            <span class="input-group-text">Contoh: 085850000000</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-12">
                    <div class="form-material form-material-primary floating input-group">
                        <input value="{{ $petugas->alamat }}" type="text" class="form-control" id="alamat" name="alamat">
                        <label for="alamat">Alamat</label>
                        <div class="input-group-append">
                            <span class="input-group-text">Contoh: Desa Rejeni RT 12 RW 06 Krembung-Sidoarjo</span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group col-md-5">
                <label class="font-tipis" for="sd_uasbn">Latitude</label>
                <input required="" value="{{ $petugas->latitude }}" type="text" class="form-control" id="latitude" name="sd_latitude_siswa">
            </div>
            <div class="form-group col-md-5">
                <label class="font-tipis" for="sd_nama_siswa">Longitude</label>
                <input required="" value="{{ $petugas->longitude }}" type="text" class="form-control" id="longitude" name="sd_longitude_siswa">
            </div>
            <div class="form-group col-md-2">
                <button type="button" class="btn btn-primary pull-right font-putih" onclick="initMap()" style="margin-top: 26.4333px; width: 100%;">Cari</button>
            </div>
            <div class="form-group col-md-12">
                <div id="map" style="height: 500px"></div>
            </div>

            <hr/><div class="row mb-2">
                <div class="col-3"></div>
                    <a class="col-3 btn btn-danger" href="{{ base_url('PetugasController/indexProfil') }}">Cancel</a>&nbsp
                    <button type="submit" class="col-3 btn bg-earth text-white">Submit</button>
                <div class="col-3"></div>
            </div>
        </form>
    </div>
</div>
<script>
    function handleEvent(event) {
        document.getElementById('latitude').value = event.latLng.lat();
        document.getElementById('longitude').value = event.latLng.lng();
    }

    function initMap() {
        var latnya = document.getElementById('latitude').value;
        var longinya = document.getElementById('longitude').value;

        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: {lat: parseFloat(latnya), lng: parseFloat(longinya)}
        });
        var geocoder = new google.maps.Geocoder();

        var marker = new google.maps.Marker({
            map: map,
            draggable: true,
            position: {lat: parseFloat(latnya), lng: parseFloat(longinya)}
        });

        marker.addListener('drag', handleEvent);
    }
</script>
<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCTnAg_gwZ-GQxB6xC0h2cY4TDFYU28ov8&callback=initMap">
</script>

@endsection